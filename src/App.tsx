import { useState } from 'react';
import './app.scss';
import fetchWeather from './api/fetchWeather';
import WeatherData from './types';

const App = () => {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState<WeatherData>();

  const search = async (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      const data = await fetchWeather(query);
      setWeather(data);
    }
  };

  return (
    <div className="mainContainer">
      <input
        type="text"
        className="search"
        placeholder="Search..."
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        onKeyDown={search}
      />
      {weather?.main && (
        <div className="city">
          <h2 className="cityName">
            <span>{weather.name}</span>
            <sup>{weather.sys.country}</sup>
          </h2>
          <div className="cityTemp">
            {Math.round(weather.main.temp)}
            <sup>&deg;C</sup>
          </div>
          <div className="info">
            <img
              src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`}
              alt={weather.weather[0].description}
              className="cityIcon"
            />
            <p>{weather.weather[0].description}</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default App;
